﻿using System;

namespace TestConsole.LeetCode
{
    public class _01TwoSum
    {
        public void Main()
        {
            // [0,1]
            this.TwoSum(new int[] { 2, 7, 11, 15 }, 9);
        }

        public int[] TwoSum(int[] nums, int target)
        {
            var tempIndexs = new int[nums.Length];
            var index = 0;
            var tempValue = new int[nums.Length];
            for (var i = 0; i < nums.Length; i++)
            {
                if (i != 0)
                {
                    for (var j = 0; j < index; j++)
                    {
                        if (tempValue[j] == nums[i])
                        {
                            return new int[] { tempIndexs[j], i };
                        }
                    }
                }
                tempIndexs[index] = i;
                tempValue[index] = target - nums[i];
                index++;
            }
            throw new Exception("");
        }
    }
}
