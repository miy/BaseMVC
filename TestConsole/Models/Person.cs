﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestConsole.MiyExtensions;

namespace TestConsole.Models
{
    public class Person
    {
        public int Id { set; get; }

        [MyDescription("姓名")]
        public string Name { set; get; }

        [MyDescription("活躍朝代")]
        public string Dynasty { set; get; }

        public string Deeds { set; get; }
    }

}
