﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole.MiyExtensions
{
    public static class DescriptionExtension
    {
        public static IEnumerable<string> GetDescription<T>()
        {
            var members = typeof(T).GetMembers();
            foreach (var item in members)
            {
                var attributes = item.GetCustomAttributes(typeof(MyDescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    var description = ((MyDescriptionAttribute)attributes[0])?.Description;
                    yield return description;
                }
            }
        }
    }

    public class MyDescriptionAttribute : Attribute
    {
        public string Description { get; set; }

        public MyDescriptionAttribute(string Description)
        {
            this.Description = Description;
        }

        public override string ToString()
        {
            return this.Description.ToString();
        }
    }
}
