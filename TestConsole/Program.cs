﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TestConsole.AsyncTest;
using TestConsole.MiyExtensions;
using TestConsole.Models;
using TestConsole.Pattren;

namespace TestConsole
{
    static class Program
    {
        static void Main(string[] args)
        {
            new LeetCode._10_Regular_Expression_Matching().Main();
            //Console.WriteLine(new FibonacciSequence().GetNumber(5));
            //new ExpressionTest().LambdaExpressionTestExecute();
            //new ObserverPatternTest().Start();
            //new LoginAsyncTest().Main();
            //new Jx3().Start();
            //Normal();
            //Test();
            //StartStreamTest();
            //new StreamTest().CreateOrWrite();
            //new DecroratorPattern().StartTest();
            //new TaskAsync().StartThread(5);
            //for (int i = 0; i < 20; i++)
            //{
            //    new ClusterAlgorithm().KMeansPlus().ForEach(x =>
            //    {
            //        Console.WriteLine(string.Join(",", x.Careers.Select(y => y.Name)));
            //    });
            //    Console.WriteLine("*****");
            //}
            //new YieldTest().Test3();
            //StartFileStream();
        }

        private static void Normal()
        {
            Console.WriteLine("hi miy,input mode first.");
            Console.WriteLine("-test -game -loop -search -ml -crawler -ef");
            string mode;
            mode = Console.ReadLine();
            switch (mode)
            {
                case "test":
                    StartTest();
                    break;
                case "game":
                    StartGame();
                    break;
                case "loop":
                    StartForLoop();
                    break;
                case "search":
                    StartSearch();
                    break;
                case "ml":
                    new MLTest().InitIris();
                    new MLTest().BinaryClassification();
                    break;
                case "crawler":
                    StartCrawler();
                    break;
                case "ef":
                    StartEfPerformanceTest();
                    break;
                case "lock":
                    new LockTest().Main();
                    break;
                default:
                    break;
            }
        }
        private static void Test()
        {
            // a = b?.a => a 會重新被賦值 
            Dictionary<int, string> dicts = new Dictionary<int, string>
            {
                { 1, "i am 1" },
                { 2, "i am 2" }
            };

            var lists = dicts.Where(x => x.Key > 1).Select(x => new Person
            {
                Id = x.Key,
                Name = "default"
            }).ToList();

            var lists2 = dicts.Select(x => new Person
            {
                Id = x.Key,
                Name = x.Value
            }).ToList();

            foreach (var item in lists2)
            {
                var temp = lists.FirstOrDefault(x => x.Id == item.Id);
                Console.WriteLine(item.Name);
                item.Name = temp?.Id.ToString();
                Console.WriteLine(item.Name);
                Console.WriteLine("------");
            }

            #region array test
            Dictionary<string, string> openWith = new Dictionary<string, string>
            {{ "txt", "notepad.exe" },{ "bmp", "paint.exe" },{ "dib", "paint.exe" },{ "rtf", "wordpad.exe" }};
            Console.WriteLine(openWith["txt"]);
            var arry1D = new int[] { 1, 2, 3, 4 };
            var arry1D2 = new int[] { 4, 7, 8, 9 };
            var arry2D = new int[4, 2];
            var arryMD = new int[][] { arry1D, arry1D2 };
            Stack<string> numbers = new Stack<string>();
            var hasSet = new HashSet<int>();
            var list = new List<int> { 1, 2, 3, 1, 5, 4, 4, 1 };
            Show(list);
            foreach (var item in arry1D.Union(arry1D2))
                hasSet.Add(item);
            hasSet.ExceptWith(arry1D);
            hasSet.RemoveWhere(x => x == 1);
            list.RemoveAll(x => x == 1);
            list.Sort((x, y) =>
            {
                if (x == y) return 0;
                else if (x > y) return 1;
                else return -1;
            });
            Show(hasSet);
            Show(list);
            #endregion
        }
        private static void StartTest()
        {
            //distinct test
            var test1 = new List<Person> {
                        new Person
                        {
                            Name = "文天祥",
                            Dynasty = "宋朝"
                        },
                        new Person
                        {
                            Name = "張世傑",
                            Dynasty = "宋朝"
                        },
                        new Person
                        {
                            Name = "文天祥",
                            Dynasty = "南宋"
                        }}.Select(x => new
                        {
                            x.Name,
                            x.Dynasty,
                            Deeds = string.Join(",", DescriptionExtension.GetDescription<Person>())
                        }).ToList();
            test1.Sort((x, y) => x.Name == "張世傑" ? 1 : -1);
            Show(test1.Distinct(x => x.Name));
            var numbers = new int[] { 1, 2, 3, 4, 5, 6 };
            Show(numbers.Where(n => n > 3));
            Console.WriteLine(numbers.Aggregate((start, next) => start + next) + " " + numbers.Sum());
            new ExpressionTest().Excute();

            Console.ReadKey();
        }
        private static void StartGame()
        {
            Game game = new Game();
            game.StartGame();
            game.StartRound();
            Console.ReadKey();
        }
        private static void StartForLoop()
        {
            string key = Console.ReadLine();
            for (var index = 0; index < 100; index++)
            {
                for (var index2 = 0; index2 < index; index2++)
                    Console.Write(key);
                Console.WriteLine();
            }
            Console.ReadKey();
        }
        private static void StartSearch()
        {
            new SearchAlgorithm().BinarySearch();
            Console.ReadKey();
        }
        private static void StartCrawler()
        {
            var url = "https://forum.gamer.com.tw/B.php?bsn=16357";
            var list = new List<object>();
            var crawler = new Crawler();
            crawler.OnStart += (s, e) =>
            {
                Console.WriteLine("Start Uri" + e.Uri.ToString());
            };
            crawler.OnError += (s, e) =>
            {
                Console.WriteLine("Catch Error" + e.Message);
            };
            crawler.OnCompleted += (s, e) =>
            {
                var links = Regex.Matches(e.PageSource,
                    @"<a[^>]+href=""*(?<URL>[^""]*).*? class=""b-list__main__title""[^>]*>(?<text>[^<]+)</a>"
                    ,
                    RegexOptions.IgnoreCase | RegexOptions.Compiled);
                foreach (Match item in links)
                {
                    var name = item.Groups["text"].Value;
                    var uri = item.Groups["URL"].Value;
                    Console.WriteLine(name);
                    Console.WriteLine("https://forum.gamer.com.tw/" + uri);
                }
                //Console.WriteLine(e.PageSource);
                Console.WriteLine("===================");
                Console.WriteLine("耗時：" + e.MilliSeconds + "毫秒");
                Console.WriteLine("線程：" + e.ThreadId);
                Console.WriteLine("Uri：" + e.Uri);
            };
            crawler.Start(new Uri(url)).Wait();
            Console.ReadKey();
        }
        private static void StartEfPerformanceTest()
        {
            var testService = new EFTest();
            int count = 10000;
            var timeWatch = new Stopwatch();
            timeWatch.Start();
            var result = testService.CreateTest(count);
            var time = timeWatch.ElapsedMilliseconds;
            timeWatch.Stop();
            if (result.Success)
            {
                Console.WriteLine("總計筆數：" + count + "筆");
                Console.WriteLine("總計耗時：" + time + "毫秒");
            }
            else
                Console.WriteLine(result.Message);
            Console.ReadKey();
        }
        private static void StartStreamTest()
        {
            //ThreadPool.SetMaxThreads(5, 5);
            ShowMessage("Main threadId is:", false);
            #region 非同步線程池
            for (var i = 0; i < 100; i++)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback((state) =>
                {
                    ShowMessage((string)state + " threadId is:", false);
                    ShowMessage((string)state + " thread close");
                }), "PoolAsync(" + i + ")");
            }
            #endregion
            #region 非同步線程
            Thread thread = new Thread(new ThreadStart(() =>
            {
                try
                {
                    ShowMessage("Async threadId is:");
                    for (int n = 0; n < 10; n++)
                    {
                        //当n等于4时，终止线程
                        if (n >= 4)
                        {
                            Thread.CurrentThread.Abort(n);
                        }
                        Console.WriteLine("The number is:" + n.ToString());
                    }
                }
                catch (ThreadAbortException ex)
                {
                    //输出终止线程时n的值
                    if (ex.ExceptionState != null)
                        Console.WriteLine(string.Format("Thread abort when the number is: {0}!",
                                                         ex.ExceptionState.ToString()));

                    //取消终止，继续执行线程
                    Thread.ResetAbort();
                    Console.WriteLine("Async Thread ResetAbort!");
                }
                //线程结束
                Console.WriteLine("Async Thread Close!");
            }))
            {
                IsBackground = true
            };
            //thread.Start();
            //thread.Join();
            #endregion
            //WaitHandle.WaitAll();
            ShowMessage("Main thread close");
        }
        private static void ShowMessage(string data, bool isDelay = true)
        {
            if (isDelay)
                Thread.Sleep(10000);
            string message = string.Format(data + " threadId is:{0}",
                Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine(message);
        }
        private static void Show<T>(IEnumerable<T> lists)
        {
            void _Show()
            {
                foreach (var item in lists)
                    Console.WriteLine(item);
                Console.WriteLine("----");
            }
            _Show();
        }

        /// <summary>
        /// 測試檔案類型之hex簽名
        /// </summary>
        private static void StartFileStream()
        {
            string[] paths = { @"C:\Users\kamis\Pictures\images.jpg", @"C:\Users\kamis\Pictures\images.pdf", @"C:\Users\kamis\Pictures\images.xlsx" };
            string mimeType = MimeMapping.GetMimeMapping(paths[2]);

            List<string> result = new List<string>();
            foreach (var path in paths)
            {
                using (FileStream fs = new FileStream(path, FileMode.Open))
                {
                    int hexIn;
                    string hex = string.Empty;

                    while ((hexIn = fs.ReadByte()) != -1)
                    {
                        hex += string.Format("{0:X2}", hexIn);
                    }

                    result.Add(hex);
                }
            }
        }
    }
}
