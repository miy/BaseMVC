﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testconsole
{
    class Program
    {
        static void Main(string[] args)
        {
            #region array test
            //Dictionary<string, string> openWith = new Dictionary<string, string>
            //{{ "txt", "notepad.exe" },{ "bmp", "paint.exe" },{ "dib", "paint.exe" },{ "rtf", "wordpad.exe" }};
            //Console.WriteLine(openWith["txt"]);
            //var arry1D = new int[] { 1, 2, 3, 4 };
            //var arry1D2 = new int[] { 4, 7, 8, 9 };
            //var arry2D = new int[4, 2];
            //var arryMD = new int[][] { arry1D, arry1D2 };
            //Stack<string> numbers = new Stack<string>();
            //var hasSet = new HashSet<int>();
            //var list = new List<int> { 1, 2, 3, 1, 5, 4, 4, 1 };
            //Show(list);
            //foreach (var item in arry1D.Union(arry1D2))
            //    hasSet.Add(item);
            //hasSet.ExceptWith(arry1D);
            //hasSet.RemoveWhere(x => x == 1);
            //list.RemoveAll(x => x == 1);
            //list.Sort(delegate (int x, int y)
            //{
            //    if (x == y) return 0;
            //    else if (x > y) return 1;
            //    else return -1;
            //});
            //Show(hasSet);
            //Show(list);
            #endregion
            #region test委派
            //void Test1(object sender, EventArgs e)
            //{
            //    Console.WriteLine("This is Test1.");
            //}

            //void Test2(object sender, EventArgs e)
            //{
            //    Console.WriteLine("This is Test2.");
            //}

            //var myDele = new Delegate();
            //myDele.InitEvent += new OnInit(Test1);
            //myDele.InitEvent += new OnInit(Test2);
            //myDele.Init();
            #endregion
            #region test call by ref
            var testClass = new TestClass();
            testClass.Add(100);
            testClass.Add(100);
            testClass.Add(100);
            testClass.Add(100);
            Console.WriteLine(testClass.Number);
            #endregion
            //Console.WriteLine(new TicketCheckHelper().GetCheckNumber("A100987638"));
            var data = new AreaService().GetAppAreaInfo<AreaLayerInfoModel>().Take(10);
            foreach (var item in data)
            {
                Console.WriteLine(item.AreaTitle + ":" + item.ListArea);
            }
        }

        class TestClass
        {
            public int Number { get; private set; } = 0;

            public void Add(int number)
            {
                Number += number;
            }
        }

        private static void Show(IEnumerable<int> lists)
        {
            void _Show()
            {
                foreach (var item in lists)
                    Console.WriteLine(item);
                Console.WriteLine("----");
            }
            _Show();
        }
    }
}
