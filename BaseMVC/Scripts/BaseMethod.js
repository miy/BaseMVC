﻿var BaseMethod = function () {
    var _listenZipCodeDOMChange = function (Dom, AddressDom) {

        Dom.find('select').on('change', function () {
            Dom.twzipcode('get', function (county, district, zipcode) {
                AddressDom.val(county + district);
            });
        })
    };
    //模擬表單submit
    var _submit = function (path, params, method) {
        method = method || "post";

        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for (var key in params) {
            if (params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
            }
        }
        document.body.appendChild(form);
        form.submit();
    };
    return {
        listenZipCodeDOMChange: function (Dom,AddressDom) {
            _listenZipCodeDOMChange(Dom,AddressDom);
        },
        SubmitByPost: function (path, params, method) {
            _submit(path, params, method);
        }
    };
}();

var CustomerHomeCareEnumMethod = function () {
    var _TransEnumValueToBoolean = function (arr) {
        $.each(arr, function (item, i) {
            item.value = Boolean(item.value);
        });
        return arr;
    };
    return {
        TransEnumValueToBoolean: function (arr) {
            return _TransEnumValueToBoolean(arr);
        }
    };
}();