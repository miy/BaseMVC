﻿var InitFiles = function () {
    Vue.component('Files', {
        props: ['value'],
        template: '<input type="file" />',
        mounted: function () {
            let vm = this
            $(this.$el).change(function () {
                vm.readURL(this);
            });
        },
        methods: {
            readURL: function (input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        }
    });
};

var InitTimePickers = function () {
    Vue.component('timepicker', {
        props: ['value'],
        template: '<input type="text" />',
        mounted: function () {
            let vm = this
            $(this.$el).timepicker({
                format: 'HH:mm',
                onSelect: function (date) {
                    vm.$emit('input', date)
                }
            })
        }
    })
};

var InitDateTimePickers = function () {
    Vue.component('datetimepicker', {
        props: ['value'],
        template: '<input type="text" />',
        mounted: function () {
            let vm = this;
            $(this.$el).datetimepicker({
                format: 'YYYY/MM/DD',
            });
            $(this.$el).on("dp.change", function (e) {
                vm.$emit('input', $(this).val());
            });
        }
    })
};

var MiyVueVer = function () {
    var Isver = 0;
    var onVerdoms = null;
    function directive(form) {
        Vue.directive('ver', {
            bind: function (el, binding, vnode) {
                bindError(el, binding, vnode);
            },
            update: function (el, binding, vnode) {
                addError(el, binding, vnode);
            }
        });
        setTimeout(function () {
            onVerdoms = form ? document.querySelectorAll(form + " .ismiyver") : null;
        }, 100);
    };
    function extend(des, src, override) {
        if (src instanceof Array) {
            for (var i = 0, len = src.length; i < len; i++)
                extend(des, src[i], override);
        }
        for (var i in src) {
            if (override || !(i in des)) {
                des[i] = src[i];
            }
        }
        return des;
    };
    function bindError(el, binding, vnode) {
        el.classList.add('ismiyver');
        let rule = eval('regList.' + binding.arg + '.rule');
        if (binding.value !== binding.oldValue) {
            if (rule.test(binding.value) === false) {
                el.classList.add('text-danger');
                Isver = Isver - 1;
            }
        }
    };
    function addError(el, binding, vnode) {
        let rule = eval('regList.' + binding.arg + '.rule');
        //for(var $dom of onVerdoms) {
        if (binding.value !== binding.oldValue) {
            if (rule.test(binding.value) === false) {
                el.classList.add('text-danger');
                showError(el, binding);
                Isver = Isver - 1;
            } else {
                if (el.classList.contains('text-danger')) {
                    el.classList.remove('text-danger');
                    el.style.borderColor = '';
                    let dom = document.getElementById(el.id + 'err');
                    if (dom) {
                        el.parentElement.removeChild(dom);
                    }
                    Isver = Isver + 1;
                }
            }
            //}
        }
    };
    function showError(el, binding) {
        el.style.borderColor = '#a94442';
        let dom = document.createElement('span');
        dom.classList.add('text-danger');
        dom.id = el.id + 'err';
        dom.innerHTML = binding ? eval('regList.' + binding.arg + '.message') : '';
        el.parentElement.appendChild(dom);
    };

    var regList = {
        required: {
            rule: /./,
            message: '此欄位必填.'
        }
    }
    return {
        Init: function (option) {
            if (option && option.regList) {
                this.ExpendregList(option.regList);
            }
            directive(option.el);
        },
        CheckIsver: function (mode) {
            if (mode == 'strict') {
                if (Isver == 0) {
                    return true;
                }
            } else {
                if (Isver >= 0) {
                    return true;
                }
            }

            onVerdoms = document.querySelectorAll('.ismiyver');
            for(dom of onVerdoms) {
                if (dom.classList.contains('text-danger')) {
                    showError(dom);
                    dom.focus();
                    break;
                }
            }
            return false;
        },
        ExpendregList: function (reg) {
            if (reg) {
                regList = extend({}, [regList, reg]);
            }
        }
    };
}();

var MiyPagelist = function () {

    var Option = {
        _PageLength: 0,
        _MaxItemsOnePage: 5,
        _Search: function () { },
        _afterLoad: function () { },
        _onChangeMaxItemsOnePage: function () { },
        _PageSize: false,
        _readMode: 'batch'
    };

    var insideParameter = {
        readCount: 0
    }

    var _miyPagelist;

    var htmlTemplete = {
        pageSize: '<div v-if="PageSize"><br />單頁顯示<input type="number" min="1" style="width:35px" v-model.lazy="MaxItemsOnePage" v-on:change="onChangeMaxItemsOnePage()" />筆</div>',
        customerTemplete: ''
    };

    var commponent = Vue.component('pagelist', {
        template: '<div aria-label="Page navigation" v-if="RealPageLength>0"><ul class="pagination">'
        + '<li v-once><a aria-label="上一頁" v-on:click="Prev()" href="javascript:void(0)"><span aria-hidden="true">«</span></a></li>'
        + '<li v-if="MinPage>11"><a href="javascript:void(0)" v-on:click="ChangePage(1)">1</a></li>'
        + '<li v-if="MinPage>1"><a href="javascript:void(0)">..</a></li></ul>'
        + '<ul class="pagination"><li v-for="(item,index) in MaxPage" :key="item" :class="{active:item==NowPage&&item >=MinPage}">'
        + '<a href="javascript:void(0)" v-if="item==NowPage&&item >=MinPage">{{item}}</a>'
        + '<a href="javascript:void(0)" v-if="item!=NowPage&&item >=MinPage" v-on:click="ChangePage(index+1)">{{item}}</a></li></ul>'
        + '<ul class="pagination"><li v-if="MaxPage<RealPageLength"><a href="javascript:void(0)">..</a></li>'
        + '<li v-if="MaxPage<RealPageLength-10"><a v-on:click="ChangePage(RealPageLength)" href="javascript:void(0)">{{RealPageLength}}</a></li>'
        + '<li v-once><a href="javascript:void(0)" v-on:click="Next()" aria-label="下一頁"><span aria-hidden="true">»</span></a></li></ul>'
        + htmlTemplete.pageSize
        + htmlTemplete.customerTemplete
        + '{{outSidePageLength}}<input type="number" v-model="a">{{b}}</div>',
        data: function () {
            return {
                PageData: [],
                PageLength: Option._PageLength,
                NowPage: 1,
                MinPage: 1,
                MaxItemsOnePage: Option._MaxItemsOnePage,
                MaxPageButton: 10,
                PageSize: Option._PageSize
            }
        },
        props: {
            a: Number,
            b: String,
            outSidePageLength: Number
        },
        computed: {
            MaxPage: function () {
                if (this.MinPage + 10 - 1 > this.RealPageLength) {
                    return this.RealPageLength;
                } else {
                    return this.MinPage + 10 - 1;
                }
            },
            RealPageLength: function () {
                return Math.floor(((this.PageLength - 1) / this.MaxItemsOnePage) + 1);
            }
        },
        watch: {
            MaxItemsOnePage: {
                handler: function (newValue) {
                    if (this.NowPage > this.MaxPage) {
                        this.ChangePage(this.MaxPage);
                    }
                },
                deep: true
            }
        },
        methods: {
            Prev: function (page) {
                page = (typeof page !== 'undefined') ? page : 1;

                if (this.NowPage > 1) {
                    this.NowPage = this.NowPage - 1;
                    if (this.MinPage > 1) {
                        this.MinPage = this.MinPage - 1;
                    }
                    this.Search();
                }
            },
            Next: function () {
                if (this.NowPage < this.RealPageLength) {
                    this.NowPage = this.NowPage + 1;
                    if (this.MaxPage < this.RealPageLength) {
                        this.MinPage = this.MinPage + 1;
                    }
                    this.Search();
                }
            },
            ChangePage: function (index) {
                this.NowPage = index;
                let maxNum = 5;
                let minNum = 3;
                let n = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum;
                let NewMinPage = index - n;
                if ((NewMinPage <= 0)) {
                    NewMinPage = 1;
                }
                this.MinPage = NewMinPage;
                this.Load();
            },
            Load: function () {
                if (Option._readMode == 'batch' || (Option._readMode == 'all' && insideParameter.readCount == 0)) {
                    insideParameter.readCount++;
                    this.Search();
                }
                this.afterLoad();
            },
            afterLoad: function () {
                Option._afterLoad();
            },
            Search: function () {
                Option._Search();
                console.log('search.' + this.NowPage);
            },
            onChangeMaxItemsOnePage: function () {
                if (this.MaxItemsOnePage <= 0) {
                    this.MaxItemsOnePage = 1;
                }
                Option._onChangeMaxItemsOnePage();
            },
            setPageLength: function (length) {
                this.PageLength = length;
            },
            getNowPage: function () {
                return this.NowPage;
            },
            setMaxItemsOnePag: function (number) {
                this.MaxItemsOnePage = number;
            },
            getMaxItemsOnePage: function () {
                return this.MaxItemsOnePage;
            }
        }
    })

    var _Init = function (option) {
        Option._MaxItemsOnePage = option ? (option.MaxItemsOnePage ? option.MaxItemsOnePage : Option._MaxItemsOnePage) : Option._MaxItemsOnePage;
        Option._Search = option.Search || Option._Search;
        Option._afterLoad = option.afterLoad || Option._afterLoad;
        Option._onChangeMaxItemsOnePage = option ? (option.onChangeMaxItemsOnePage ? option.onChangeMaxItemsOnePage : null) : null;
        Option._PageSize = option ? (option.PageSize ? option.PageSize : Option._PageSize) : Option._PageSize;
        Option._readMode = option.readMode || Option._readMode;

        return commponent;
    };

    return {
        Init: function (option) {
            _Init(option);
        },
        bind: function (vm) {
            _miyPagelist = vm.$refs.miy_pagelist;
        },
        setPageLength: function (length) {
            _miyPagelist.setPageLength(length);
        },  
        getNowPage: function () {
            return _miyPagelist.getNowPage();
        },
        setMaxItemsOnePage: function (number) {
            _miyPagelist.setMaxItemsOnePag(number);
        },
        getMaxItemsOnePage: function () {
            return _miyPagelist.getMaxItemsOnePage();
        },
        getDisplayData: function (data) {
            let nowPage = this.getNowPage();
            let maxItemsOnePage = this.getMaxItemsOnePage();
            return data.slice((nowPage - 1) * maxItemsOnePage, nowPage * maxItemsOnePage);
        }
    };
}();

class NewMiyPagelist {
    constructor(option) {
        this._option = {
            PageLength: 0,
            MaxItemsOnePage: 5,
            Search: function () { },
            AfterLoad: function () { },
            OnChangeMaxItemsOnePage: function () { },
            PageSize: false,
            ReadMode: 'batch'
        };
        this._insideParameter = 0;
        this._miyPagelist;
        this.htmlTemplete = {
            pageSize: '<div v-if="PageSize"><br />單頁顯示<input type="number" min="1" style="width:35px" v-model.lazy="MaxItemsOnePage" v-on:change="onChangeMaxItemsOnePage()" />筆</div>',
            customerTemplete: ''
        };
        this.commponent = Vue.component('pagelist', {
            template: '<div aria-label="Page navigation" v-if="RealPageLength>0"><ul class="pagination">'
            + '<li v-once><a aria-label="上一頁" v-on:click="Prev()" href="javascript:void(0)"><span aria-hidden="true">«</span></a></li>'
            + '<li v-if="MinPage>11"><a href="javascript:void(0)" v-on:click="ChangePage(1)">1</a></li>'
            + '<li v-if="MinPage>1"><a href="javascript:void(0)">..</a></li></ul>'
            + '<ul class="pagination"><li v-for="(item,index) in MaxPage" :key="item" :class="{active:item==NowPage&&item >=MinPage}">'
            + '<a href="javascript:void(0)" v-if="item==NowPage&&item >=MinPage">{{item}}</a>'
            + '<a href="javascript:void(0)" v-if="item!=NowPage&&item >=MinPage" v-on:click="ChangePage(index+1)">{{item}}</a></li></ul>'
            + '<ul class="pagination"><li v-if="MaxPage<RealPageLength"><a href="javascript:void(0)">..</a></li>'
            + '<li v-if="MaxPage<RealPageLength-10"><a v-on:click="ChangePage(RealPageLength)" href="javascript:void(0)">{{RealPageLength}}</a></li>'
            + '<li v-once><a href="javascript:void(0)" v-on:click="Next()" aria-label="下一頁"><span aria-hidden="true">»</span></a></li></ul>'
            + this.htmlTemplete.pageSize
            + this.htmlTemplete.customerTemplete
            + '{{outSidePageLength}}</div>',
            data: function () {
                return {
                    PageData: [],
                    PageLength: _option.PageLength,
                    NowPage: 1,
                    MinPage: 1,
                    MaxItemsOnePage: _option.MaxItemsOnePage,
                    MaxPageButton: 10,
                    PageSize: _option.PageSize,
                }
            },
            props: ['outSidePageLength'],
            computed: {
                MaxPage: function () {
                    if (this.MinPage + 10 - 1 > this.RealPageLength) {
                        return this.RealPageLength;
                    } else {
                        return this.MinPage + 10 - 1;
                    }
                },
                RealPageLength: function () {
                    return Math.floor(((this.PageLength - 1) / this.MaxItemsOnePage) + 1);
                }
            },
            watch: {
                MaxItemsOnePage: {
                    handler: function (newValue) {
                        if (this.NowPage > this.MaxPage) {
                            this.ChangePage(this.MaxPage);
                        }
                    },
                    deep: true
                }
            },
            methods: {
                Prev: function (page) {
                    page = (typeof page !== 'undefined') ? page : 1;

                    if (this.NowPage > 1) {
                        this.NowPage = this.NowPage - 1;
                        if (this.MinPage > 1) {
                            this.MinPage = this.MinPage - 1;
                        }
                        this.Search();
                    }
                },
                Next: function () {
                    if (this.NowPage < this.RealPageLength) {
                        this.NowPage = this.NowPage + 1;
                        if (this.MaxPage < this.RealPageLength) {
                            this.MinPage = this.MinPage + 1;
                        }
                        this.Search();
                    }
                },
                ChangePage: function (index) {
                    this.NowPage = index;
                    let maxNum = 5;
                    let minNum = 3;
                    let n = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum;
                    let NewMinPage = index - n;
                    if ((NewMinPage <= 0)) {
                        NewMinPage = 1;
                    }
                    this.MinPage = NewMinPage;
                    this.Load();
                },
                Load: function () {
                    if (Option._readMode == 'batch' || (Option._readMode == 'all' && insideParameter.readCount == 0)) {
                        insideParameter.readCount++;
                        this.Search();
                    }
                    this.afterLoad();
                },
                afterLoad: function () {
                    Option._afterLoad();
                },
                Search: function () {
                    Option._Search();
                    console.log('search.' + this.NowPage);
                },
                onChangeMaxItemsOnePage: function () {
                    if (this.MaxItemsOnePage <= 0) {
                        this.MaxItemsOnePage = 1;
                    }
                    Option._onChangeMaxItemsOnePage();
                },
                setPageLength: function (length) {
                    this.PageLength = length;
                },
                getNowPage: function () {
                    return this.NowPage;
                },
                setMaxItemsOnePag: function (number) {
                    this.MaxItemsOnePage = number;
                },
                getMaxItemsOnePage: function () {
                    return this.MaxItemsOnePage;
                }
            }
        });

        this.Init(option);
    }

    Init(option) {
        this._option.MaxItemsOnePage = option.MaxItemsOnePage || this._option.MaxItemsOnePage;
        this._option.Search = option.Search || this._option.Search;
        this._option.AfterLoad = option.AfterLoad || this._option.AfterLoad;
        this._option.OnChangeMaxItemsOnePage = option.OnChangeMaxItemsOnePage || this._option.OnChangeMaxItemsOnePage;
        this._option.PageSize = option.PageSize || this._option.PageSize;
        this._option.ReadMode = option.ReadMode || this._option.ReadMode;

        return this.commponent;
    }

    bind(vm) {
        this._miyPagelist = vm.$refs.miy_pagelist;
    }

    getDisplayData(data) {
        let nowPage = this.nowPage;
        let maxItemsOnePage = this.maxItemsOnePage;
        return data.slice((nowPage - 1) * maxItemsOnePage, nowPage * maxItemsOnePage);
    }

    set pageLength(length) {
        this._miyPagelist.PageLength = length;
    }

    get nowPage() {
        return this._miyPagelist.NowPage;
    }

    set maxItemsOnePage(number) {
        this._miyPagelist.MaxItemsOnePage = number;
    }

    get maxItemsOnePage() {
        return this._miyPagelist.MaxItemsOnePage;
    }
}

var InitZipCode = function () {
    Vue.component('zipcode', {
        props: ['value'],
        template: '<div></div>',
        mounted: function () {
            let vm = this;
            let el = this.$el;
            $(this.$el).twzipcode({
                'onDistrictSelect': function () {
                    $(el).twzipcode('get', function (county, district, zipcode) {
                        vm.$emit('input', zipcode);
                    });
                }
            });
            $(this.$el).twzipcode('set', {
                'zipcode': this.value
            });
        }
    })
};