﻿

var MiyPagelist = function () {

    var Option = {
        _PageLength: 0,
        _MaxItemsOnePage: 5,
        _Search: function () { },
        _afterLoad: function () { },
        _onChangeMaxItemsOnePage: function () { },
        _PageSize: false,
        _readMode: 'batch'
    };

    var insideParameter = {
        readCount: 0
    }

    var _miyPagelist;

    var htmlTemplete = {
        pageSize: '<div v-if="PageSize"><br />單頁顯示<input type="number" min="1" style="width:35px" v-model.lazy="MaxItemsOnePage" v-on:change="onChangeMaxItemsOnePage()" />筆</div>',
        customerTemplete: ''
    };

    var commponent = Vue.component('pagelist', {
        template: '<div aria-label="Page navigation" v-if="RealPageLength>0"><ul class="pagination">'
        + '<li v-once><a aria-label="上一頁" v-on:click="Prev()" href="javascript:void(0)"><span aria-hidden="true">«</span></a></li>'
        + '<li v-if="MinPage>11"><a href="javascript:void(0)" v-on:click="ChangePage(1)">1</a></li>'
        + '<li v-if="MinPage>1"><a href="javascript:void(0)">..</a></li></ul>'
        + '<ul class="pagination"><li v-for="(item,index) in MaxPage" :key="item" :class="{active:item==NowPage&&item >=MinPage}">'
        + '<a href="javascript:void(0)" v-if="item==NowPage&&item >=MinPage">{{item}}</a>'
        + '<a href="javascript:void(0)" v-if="item!=NowPage&&item >=MinPage" v-on:click="ChangePage(index+1)">{{item}}</a></li></ul>'
        + '<ul class="pagination"><li v-if="MaxPage<RealPageLength"><a href="javascript:void(0)">..</a></li>'
        + '<li v-if="MaxPage<RealPageLength-10"><a v-on:click="ChangePage(RealPageLength)" href="javascript:void(0)">{{RealPageLength}}</a></li>'
        + '<li v-once><a href="javascript:void(0)" v-on:click="Next()" aria-label="下一頁"><span aria-hidden="true">»</span></a></li></ul>'
        + htmlTemplete.pageSize
        + htmlTemplete.customerTemplete
        + '{{outSidePageLength}}<input type="number" v-model="a">{{b}}</div>',
        data: function () {
            return {
                PageData: [],
                PageLength: Option._PageLength,
                NowPage: 1,
                MinPage: 1,
                MaxItemsOnePage: Option._MaxItemsOnePage,
                MaxPageButton: 10,
                PageSize: Option._PageSize
            }
        },
        props: {
            a: Number,
            b: String,
            outSidePageLength: Number
        },
        computed: {
            MaxPage: function () {
                if (this.MinPage + 10 - 1 > this.RealPageLength) {
                    return this.RealPageLength;
                } else {
                    return this.MinPage + 10 - 1;
                }
            },
            RealPageLength: function () {
                return Math.floor(((this.PageLength - 1) / this.MaxItemsOnePage) + 1);
            }
        },
        watch: {
            MaxItemsOnePage: {
                handler: function (newValue) {
                    if (this.NowPage > this.MaxPage) {
                        this.ChangePage(this.MaxPage);
                    }
                },
                deep: true
            }
        },
        methods: {
            Prev: function (page) {
                page = (typeof page !== 'undefined') ? page : 1;

                if (this.NowPage > 1) {
                    this.NowPage = this.NowPage - 1;
                    if (this.MinPage > 1) {
                        this.MinPage = this.MinPage - 1;
                    }
                    this.Search();
                }
            },
            Next: function () {
                if (this.NowPage < this.RealPageLength) {
                    this.NowPage = this.NowPage + 1;
                    if (this.MaxPage < this.RealPageLength) {
                        this.MinPage = this.MinPage + 1;
                    }
                    this.Search();
                }
            },
            ChangePage: function (index) {
                this.NowPage = index;
                let maxNum = 5;
                let minNum = 3;
                let n = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum;
                let NewMinPage = index - n;
                if ((NewMinPage <= 0)) {
                    NewMinPage = 1;
                }
                this.MinPage = NewMinPage;
                this.Load();
            },
            Load: function () {
                if (Option._readMode == 'batch' || (Option._readMode == 'all' && insideParameter.readCount == 0)) {
                    insideParameter.readCount++;
                    this.Search();
                }
                this.afterLoad();
            },
            afterLoad: function () {
                Option._afterLoad();
            },
            Search: function () {
                Option._Search();
                console.log('search.' + this.NowPage);
            },
            onChangeMaxItemsOnePage: function () {
                if (this.MaxItemsOnePage <= 0) {
                    this.MaxItemsOnePage = 1;
                }
                Option._onChangeMaxItemsOnePage();
            },
            setPageLength: function (length) {
                this.PageLength = length;
            },
            getNowPage: function () {
                return this.NowPage;
            },
            setMaxItemsOnePag: function (number) {
                this.MaxItemsOnePage = number;
            },
            getMaxItemsOnePage: function () {
                return this.MaxItemsOnePage;
            }
        }
    })

    var _Init = function (option) {
        Option._MaxItemsOnePage = option ? (option.MaxItemsOnePage ? option.MaxItemsOnePage : Option._MaxItemsOnePage) : Option._MaxItemsOnePage;
        Option._Search = option.Search || Option._Search;
        Option._afterLoad = option.afterLoad || Option._afterLoad;
        Option._onChangeMaxItemsOnePage = option ? (option.onChangeMaxItemsOnePage ? option.onChangeMaxItemsOnePage : null) : null;
        Option._PageSize = option ? (option.PageSize ? option.PageSize : Option._PageSize) : Option._PageSize;
        Option._readMode = option.readMode || Option._readMode;

        return commponent;
    };

    return {
        Init: function (option) {
            _Init(option);
        },
        bind: function (vm) {
            _miyPagelist = vm.$refs.miy_pagelist;
        },
        setPageLength: function (length) {
            _miyPagelist.setPageLength(length);
        },  
        getNowPage: function () {
            return _miyPagelist.getNowPage();
        },
        setMaxItemsOnePage: function (number) {
            _miyPagelist.setMaxItemsOnePag(number);
        },
        getMaxItemsOnePage: function () {
            return _miyPagelist.getMaxItemsOnePage();
        },
        getDisplayData: function (data) {
            let nowPage = this.getNowPage();
            let maxItemsOnePage = this.getMaxItemsOnePage();
            return data.slice((nowPage - 1) * maxItemsOnePage, nowPage * maxItemsOnePage);
        }
    };
}();


export { MiyPagelist } ;
