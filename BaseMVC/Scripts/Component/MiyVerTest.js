﻿import { MiyPagelist } from './BaseVueCompontent.js';
var thought = [{
    name: '儒家',
    source: '司徒',
    responsibility: '教育'
},{
    name: '道家',
    source: '史官',
    responsibility: '紀錄典籍'
},{
    name: '陰陽家',
    source: '羲和',
    responsibility: '星曆'
},{
    name: '法家',
    source: '理官',
    responsibility: '刑法'
},{
    name: '名家',
    source: '禮官',
    responsibility: '禮秩'
},
            {
                name: '墨家',
                source: '清廟之守',
                responsibility: '祀典'
            },
            {
                name: '縱橫家',
                source: '行人',
                responsibility: '朝覲聘問'
            },
            {
                name: '雜家',
                source: '議官',
                responsibility: '議郎'
            },
            {
                name: '農家',
                source: '農稷',
                responsibility: '教民稼穡'
            },
            {
                name: '小說家',
                source: '稗官',
                responsibility: ''
            }];

var app = new Vue({
    el: '#pageTest',
    data: {
        searchInput: {
            pageCnt: 1,
            maxItemsOnePage: 5
        },
        thought: [],
        miyPageList:''
    },
    beforeCreate: function () {
        let vm = this;
        MiyPagelist.Init({
            Search: vm.search,
            afterLoad: function () {
                //vm.searchInput.pageCnt = MiyPagelist.getNowPage();
            },
            onChangeMaxItemsOnePage: function () {
                //vm.searchInput.maxItemsOnePage = MiyPagelist.getMaxItemsOnePage();
            },
            MaxItemsOnePage: 5,
            PageSize: true,
            readMode: 'all'
        });
    },
    created: function () {

    },
    mounted: function () {
        MiyPagelist.bind(this);
        this.search();
    },
    computed: {
        displayData: function () {
            return MiyPagelist.getDisplayData(this.thought);
        }
    },
    methods: {
        search: function () {
            MiyPagelist.setPageLength(thought.length);
            this.thought = thought;
        },
        submit: function () {

        }
    }
});