﻿using BaseMVC.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BaseMVC.Controllers
{
    public class HomeController : Controller
    {
        public string cnStr = "Persist Security Info=False;Trusted_Connection=True; MultipleActiveResultSets=True;database=BaseMVCTest;server=(local)";

        //deadlock
        [HttpGet]
        public ActionResult DownloadPage()
        {
            var task = MyDownloadPageAsync("http://huan-lin.blogspot.com");
            var content = task.Result;
            return Json(string.Format("網頁長度: {0}", content.Length), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> DownloadPage2()
        {
            var task = MyDownloadPageAsync("http://huan-lin.blogspot.com");
            var content = await task;
            return Json(string.Format("網頁長度: {0}", content.Length), JsonRequestBehavior.AllowGet);
        }

        private async Task<string> MyDownloadPageAsync(string url)
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                var task = client.GetStringAsync(url);
                // 這裡會獲取當前的 SynchronizationContext 
                string content = await task; // 這裡在 task 完成後，會 deadlock!
                return content;
            }
        }

        public ActionResult Index()
        {
            var startTime = DateTime.Now.ToString("HH:mm:ss");

            Task.Run(() => { LogAsync(); });
            var endTime = DateTime.Now.ToString("HH:mm:ss");
            return Json(String.Format("{0} -- {1}", startTime, endTime), JsonRequestBehavior.AllowGet);
        }

        private async void LogAsync()
        {
            await Task.Delay(9000); // 刻意延遲九秒.
            string path = System.Environment.GetFolderPath(
                Environment.SpecialFolder.MyDocuments);
            System.IO.File.WriteAllText(path + @"\log.txt", "太虛劍意" +
                DateTime.Now.ToString("HH:mm:ss"));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult GetData()
        {
            using (var cn = new SqlConnection(cnStr))
            {
                var list = cn.Query<Models.TestModels.Dapper>(
              "SELECT * FROM dappers WHERE Id>@id", new { id = 1 });
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Create()
        {
            using (var cn = new SqlConnection(cnStr))
            {
                cn.Execute(@"INSERT INTO dappers VALUES (@name)",
                     new[] {
                        new { name = "haha" },
                        new { name = "waawa" }
                     });
                return RedirectToAction("GetData");
            }
        }
    }
}