﻿using BaseMVC.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BaseMVC.Controllers
{
    public class TestController : Controller
    {
        private static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<TestHub>();

        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Client2()
        {
            return View();
        }

        public ActionResult Knock()
        {
            var servents = TestHub.AllServentList;
            var servent = servents.FirstOrDefault(x => x.Code.Contains("貞德"));
            hubContext.Clients.All.sendAllMessge("麻婆不想說話，並向你丟了豆腐");
            if (servent != null)
            {
                hubContext.Clients.Client(servent.ConnectionID).sendAllMessge("背後突然一陣聲響，聖女大人啊啊啊啊啊啊啊！");
            }
            return View();
        }

        public ActionResult MiyVerTest()
        {
            return View();
        }

        public ActionResult GetAlang()
        {
            var a = new Alang();


            var b = new Alang2();
            var c = new Alang3();
            var d = new Alang4();
            var tt = d._iamfield;
            return Json(new
            {
                a,
                d,
                b,
                c,
                tt = tt
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult JsImport()
        {
            return View();
        }
    }
}