﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using BaseMVC.Models;
using Microsoft.AspNet.SignalR;

namespace BaseMVC
{
    public class TestHub : Hub
    {
        private static List<Servent> ServentList = new List<Servent>();

        public static List<Servent> AllServentList
        {
            get => ServentList;
        }

        public string CreateCode()
        {
            Random rnd = new Random();

            string code = rnd.Next().ToString().Substring(0, 4);
            do
            {
                code = rnd.Next().ToString().Substring(0, 4);

            } while (ServentList.Where(p => p.Code == code).Count() > 0);

            //先移除上次產生的號碼，如果有的話
            ServentList.RemoveAll(p => p.ConnectionID == Context.ConnectionId);
            //要求產生號碼時，紀錄連線的 ConnectionId，還有上面產生的四個不重複數字
            ServentList.Add(new Servent { ConnectionID = Context.ConnectionId, Code = code });
            //Clients.All.hello();
            return Clients.Client(Context.ConnectionId).getCode(code);
        }

        public void Hello(string name)
        {
            ServentList.RemoveAll(p => p.ConnectionID == Context.ConnectionId);
            ServentList.Add(new Servent { ConnectionID = Context.ConnectionId, Code = name });
            string message = "歡迎 " + name + " 加入本次聖杯戰爭，存活的御主尚有" + ServentList.Count() + "名";
            Clients.All.hello(new
            {
                message,
                ServentList
            });
        }

        public void SendMessage(string name, string message, string SendToID = null)
        {
            message = name + ":" + message;
            if (string.IsNullOrEmpty(SendToID))
            {
                Clients.All.sendAllMessge(message);
            }
            else
            {
                Clients.Caller.sendAllMessge(message);
                Clients.Client(SendToID).sendAllMessge(message);
            }
        }
        /// <summary>
        /// 配對
        /// </summary>
        /// <param name="code"></param>
        public void FindSameCode(string code)
        {
            var Servent = ServentList.FirstOrDefault(p => p.Code == code && p.Code2 == null);
            if (Servent == null)
            {
                Clients.Client(Context.ConnectionId).matchMsg(false);
            }
            else
            {
                Servent.ConnectionID2 = Context.ConnectionId;
                Servent.Code2 = code;
                Clients.Client(Context.ConnectionId).matchMsg(true);
                Clients.Client(Servent.ConnectionID).matchMsg(true);
            }
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return DeleteCode();
        }

        /// <summary>
        /// 斷線時清除使用者
        /// </summary>
        public Task DeleteCode()
        {
            var Servent = ServentList.FirstOrDefault(x => x.ConnectionID == Context.ConnectionId);
            ServentList.RemoveAll(p => p.ConnectionID == Context.ConnectionId);
            string message = "很遺憾 " + Servent?.Code + " 離開了本次聖杯戰爭，存活的御主尚有" + ServentList.Count() + "名";
            Clients.All.hello(new
            {
                message,
                ServentList
            });
            return null;
        }
    }
}