﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseMVC.Models
{
    public class Person
    {
        public string _iamfield = "我無法被更改";

        public string Sex { set; get; }
        public string Name { set; get; }
    }

    public class Alang : Person
    {
        public Alang()
        {
            this._iamfield = "我可以被更改";
            this.Sex = "男神";
            this.Name = "阿狼";
        }
    }

    public class Alang4 : Person
    {
        public Alang4()
        {
            this._iamfield = "我還真他媽可以被更改";
            this.Sex = "男你他媽的神";
            this.Name = "阿我她妳媽的狼";
        }
    }

    public class Person2
    {
        public string _iamfield = "我無法被更改";

        public string Sex { set; get; }
        public string Name { set; get; }

        public Person2(string Sex, string Name, string iamfield)
        {
            this.Sex = Sex;
            this.Name = Name;
            this._iamfield = iamfield;
        }
    }

    public class Alang2 : Person2
    {
        public Alang2():base("我可以被更改", "男神", "阿狼")
        {
            this.Name = "我是後來才改的阿郎";
        }
    }

    public class Alang3 : Person2 {
        public Alang3() : base("我可以他媽被更改", "男他媽神", "阿他媽狼")
        {

        }
    }
}
