namespace BaseMVC.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class edit_per : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Performances");
            AlterColumn("dbo.Performances", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Performances", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Performances");
            AlterColumn("dbo.Performances", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Performances", "Id");
        }
    }
}
