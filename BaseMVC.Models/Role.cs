﻿using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Models
{
    public class Role : IdentityRole<string, UserRole>
    {
        public Role()
        {
            this.Id = Guid.NewGuid().ToString();
        }

        public Role(string name)
            : this()
        {
            this.Name = name;
        }
    }
}