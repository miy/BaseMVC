﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Text;
using System.Web;

namespace BaseMVC.Models
{
    public enum MediaType
    {
        [Description("application/json")]
        ApplicationJson = 0,
        [Description("application/x-www-form-urlencoded")]
        ApplicationUrlencoded = 1
    }

    public class HttpClientHelper
    {
        public Result HttpGet(string url)
        {
            Result result = new Result(false);
            try
            {
                var client = new HttpClient();
                var response = client.GetAsync(url).Result;
                if (response.StatusCode.ToString() == "OK")
                {
                    result.Success = true;
                }

                result.Message = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public Result HttpGetAddAuthorization(string url, string Token, string TokenType = "JWT")
        {
            Result result = new Result(false);
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", TokenType + " " + Token);
                var response = client.GetAsync(url).Result;
                if (response.StatusCode.ToString() == "OK")
                {
                    result.Success = true;
                }

                result.Message = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Exception = ex;
            }
            return result;
        }

        public Result HttpPost(string url, string postData, MediaType mediaType = MediaType.ApplicationJson)
        {
            Result result = new Result(false);
            try
            {
                var client = new HttpClient();
                var headerMediaType = EnumDescriptionExtensions.GetDescription<MediaType>(mediaType.ToString());

                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("text/html"));
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/xhtml+xml"));
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/xml"));

                StringContent content = new StringContent(postData, Encoding.UTF8, headerMediaType);
                var response = client.PostAsync(url, content).Result;
                if (response.StatusCode.ToString() == "200" || response.StatusCode.ToString() == "OK")
                {
                    result.Success = true;
                }
                result.Message = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Exception = ex;
            }
            return result;
        }

        public Result HttpPost(string url, Dictionary<string, string> postData, MediaType mediaType = MediaType.ApplicationUrlencoded)
        {
            Result result = new Result(false);
            try
            {
                var client = new HttpClient();
                var headerMediaType = EnumDescriptionExtensions.GetDescription<MediaType>(mediaType.ToString());

                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("text/html"));
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/xhtml+xml"));
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/xml"));

                if (mediaType == MediaType.ApplicationUrlencoded)
                {
                    var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(postData) };
                    var response = client.SendAsync(req).Result;
                    if (response.StatusCode.ToString() == "200" || response.StatusCode.ToString() == "OK")
                    {
                        result.Success = true;
                    }
                    result.Message = response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Exception = ex;
            }
            return result;
        }

    }
}